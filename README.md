# Surabi Billing Management System

## Steps to run:-

1. Clone and import it as a maven project.
2. Open MySql Command line tool and run.

   > source <path_to_surabi_sql_file/Surabi.sql>

3. Open src/main/java com.elan.Database.ThreadSafeConnectionSingleton class and alter the username and password according to your database user.
4. And then run src/main/java com.elan.Surabi.App

## Key points:-

1. In this application I've categorized users into customer(a normal user) and admin.
2. All the users created through java application are of type customer.
3. If you want to perform admin operation use predefined admin users from database or directly insert a new admin user into database.
4. username and password are case insensitive.

## Design Patterns Used:-

1. Singleton
   - For creating Database connection Thread safe singleton design pattern is used.
   - Only one instance connection object is used by all the threads in the application.
2. Facade
   - Facade design pattern is used hide the complex implementation from users and provide them with a simple interface. (Customer and Admin Controller)
3. Factory
   - Factory design pattern is used to get the controllers based on user type (Customer or Admin).
   - This allows us to hide business logic (DAO classes) from the client.

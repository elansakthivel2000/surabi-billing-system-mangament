CREATE DATABASE IF NOT EXISTS Surabi;

USE Surabi;

DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS bills;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS products;

-- BOTH NORMAL USER AND ADMIN ARE STORED HERE. THEY'RE DIFFERENTIATED USING THE ROLE FIELD.
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(40) UNIQUE,
    password VARCHAR(40),
    role ENUM('ADMIN','USER') NOT NULL DEFAULT 'USER',
    created_at TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY(id)
);

-- INSERTING SOME PREDEFINED USERS 
-- ADMINS CAN ONLY BE CREATED BY DIRECTLY INSERTING INTO DATABASE AND NOT THROUGH JAVA APPLICATION
INSERT INTO users
(username, password, role)
VALUES
('Elan','elan#1234','ADMIN'),
('Pranav','pranav#1234','ADMIN'),
('Naveen','naveen#1234', 'USER');

-- MENU ITEMS ARE STORED IN PRODUCTS TABLE
CREATE TABLE products (
    id INT NOT NULL AUTO_INCREMENT,
    product_name VARCHAR(40) UNIQUE,
    price DECIMAL(10,2),
    PRIMARY KEY(id)
);

-- INSERTING SOME PREDEFINED VALUES INTO PRODUCTS
INSERT INTO products
(product_name, price)
VALUES
('Pizza', 4.99),
('Burger', 5.49),
('Pasta', 10.09),
('Noodles', 2.99),
('Curry', 1.99);

-- BILLS TALBE IS TO STORE THE BILL DETAILS AFTER A PURCHASE IS COMPLETE.
CREATE TABLE bills (
    id INT NOT NULL AUTO_INCREMENT,
    bill_amount DECIMAL(10,2),
    num_items INT,
    purchased_by INT,
    purchased_at TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY(id),
    FOREIGN KEY(purchased_by) REFERENCES users(id)
);

-- ORDERS TABLE IS TO STORE THE EACH AND INDIVIDUAL PRODUCT ASSOSIATED WITH A BILL
CREATE TABLE orders (
    id INT NOT NULL AUTO_INCREMENT,
    product_id INT,
    bill_id INT,
    product_count INT,
    PRIMARY KEY(id),
    UNIQUE KEY(product_id, bill_id),
    FOREIGN KEY(product_id) REFERENCES products(id),
    FOREIGN KEY(bill_id) REFERENCES bills(id)
);

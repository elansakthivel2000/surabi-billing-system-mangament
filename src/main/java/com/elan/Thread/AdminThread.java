package com.elan.Thread;

import java.sql.Connection;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Product.Product;
import com.elan.Model.SalesReport.Report;
import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Classes.AdminController;
import com.elan.Surabi.Utils.AddProduct;
import com.elan.Surabi.Utils.Display;

public class AdminThread implements Runnable {
	private AdminController adminController;
	private Connection connection;
	private Scanner input;
	private User user;
	
	public AdminThread(AdminController adminController,User user, Connection connection, Scanner input) {
		this.adminController = adminController;
		this.connection = connection;
		this.input = input;
		this.user = user;
	}
	
	// this method will be called by JVM when scheduler picks this thread for execution.
	@Override
	public void run() {
//		System.out.println("\n>> AdminThread: Inside run methd.."); (FOR DEV PURPOSE)
		
		int decision = 2;
		// use do while loop to allow the admin to perform as many operation as he wants
		loop:
		do {
			System.out.println("\nPlease choose an option: ");
			System.out.println("1. Get Today's Bills");
			System.out.println("2. Get This Month's Total Sales Report");
			System.out.println("3. Add a Product");
			System.out.println("4. Logout");
			
			int mainOption;
			try {
				mainOption = input.nextInt();				
			} catch(InputMismatchException e) {
				System.out.println("Invalid Option: Please only use 1 or 2 to select the option.");
				System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
				break;
			}
			
			switch(mainOption) {
				// get today's bills and display it.
				case 1:
					List<Bill> todaysBills = adminController.getTodaysBills(connection);
					Display.displayBills(todaysBills);
					break;
				// get month's sales report and display it.
				case 2:
					Report report = adminController.getMonthsTotalSale(connection);
					Display.displayReport(report);
					break;
				// add new product to our database table.
				case 3:
					Product product = AddProduct.addProduct(adminController, connection, input);
					if(product == null) {
						break loop;
					}
					Display.displayProducts(product);
					break;
				// log the user out and close the application.
				case 4:
					User loggedOutUser = adminController.logout(user);
					System.out.println("User "+loggedOutUser.getUserName()+" logged out");
					break loop;
				default:
					System.out.println("Please enter a valid option");
					break;
			}
			
			System.out.println("\nDo you want to continue using the app? Choose(1/2)");
			System.out.println("1. Yes");
			System.out.println("2. No");
			try {
				decision = input.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("Invalid Option: Please only use 1 or 2 to select the option.");
				System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
				break;
			}
		} while(decision == 1);
		
	}
	// once the run method is fully executed, thread will get closed.
}

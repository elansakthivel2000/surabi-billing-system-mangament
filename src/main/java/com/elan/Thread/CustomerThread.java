package com.elan.Thread;

import java.sql.Connection;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;
import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Classes.CustomerController;
import com.elan.Surabi.Utils.Display;

public class CustomerThread implements Runnable {
	private CustomerController customerController;
	private Connection connection;
	private Scanner input;
	private User user;
	
	
	public CustomerThread(CustomerController customerController, User user, Connection connection, Scanner input) {
		this.customerController = customerController;
		this.connection = connection;
		this.input = input;
		this.user = user;
	}

	@Override
	public void run() {
//		System.out.println(">> Customer Thread: Inside run methd.."); (FOR DEV PURPOSE)
		int decision = 2;
		
		// use do while loop to allow the user to perform as many operation as he wants
		loop:
		do {
			System.out.println("\nPlease Select An Option:");
			System.out.println("1. View Products");
			System.out.println("2. Logout");
			
			int mainOption;
			try {				
				mainOption = input.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("Invalid Option: Please only use 1 or 2 to select the option.");
				System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
				break loop;
			}
			
			switch(mainOption) {
				// view the list of products
				case 1:
					List<Product> products = customerController.getProducts(connection);
					boolean isProductsListEmpty = Display.displayProducts(products);
					
					// check if the products is not empty and then show options to start adding items to cart.
					if(!isProductsListEmpty) {
						System.out.println("\nDo you want to start adding items to your Cart ? Choose(1/2)");
						System.out.println("1. Yes");
						System.out.println("2. No");
						
						int productOption;
						try {
							productOption = input.nextInt();							
						} catch(InputMismatchException e) {
							System.out.println("Invalid Option: Please only use 1 or 2 to select the option.");
							System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
							break;
						}
						
						productSwitchCase:
						switch(productOption) {
							// start adding items to cart
							case 1:
								// use hashmap to define the cart structure
								// to view the structure refer:- OrderDAO
								HashMap<Product,Integer> cart = new HashMap<>();
								
								// default case to check if user has entered an unexpected value and break the loop.
								int keepAdding = 121;
								
								// use do while loop to allow users to add more than one item
								do {
									System.out.println("\nUse the serial number to add products to your cart.");
									
									// display the list of products along with serial number.
									// later use the serial number to get the product user selected and add it to cart.
									Display.displayProducts(products);
									
									// add one more option at the end allowing user to logout.
									System.out.println("\n"+(products.size()+1)+". Logout");
									
									int cartItemNumber;
									try {
										cartItemNumber = input.nextInt();										
									} catch(InputMismatchException e) {
										System.out.println("Invalid Option: Please only use serial number to select the option.");
										System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
										break;
									}
									
									// Logout user if he chooses that option.
									if(cartItemNumber == products.size()+1) {
										User loggedOutUser = customerController.logout(user);
										System.out.println("User "+loggedOutUser.getUserName()+" logged out");
										
										// break the loop after logout.
										break loop;
									
									// if user enters a serial number which doesn't exists show error message.
									} else if(cartItemNumber > products.size()+1) {
										System.out.println("Please enter a valid serial number.");
										continue;
									}
									
									// if everything went right add that product to cart.
									Product product = products.get(cartItemNumber-1);
									cart.put(product, cart.getOrDefault(product, 0) + 1);
									
									// display the cart and get the bill back to show user the final bill, if he/she decides to checkout
									Bill bill = Display.displayCart(cart,user);
									
									// after adding one item to cart show user the option to checkout or continue shopping or logout.
									System.out.println("\nChoose an option:");
									System.out.println("1. Continue Shopping.");
									System.out.println("2. Checkout");
									System.out.println("3. Logout");
									
									try {										
										keepAdding = input.nextInt();
									} catch(InputMismatchException e) {
										System.out.println("Invalid Option: Please only use 1, 2 or 3 to select the option.");
										System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
										break;
									}
									
									// user decides to checkout.
									/*
									 * 	Checkout is a two step process:
									 * 		1. creating a bill and inserting it into bills table.
									 * 		2. and to maintain all the products associated with that bill, add entries for each
									 * 		  product in the cart in the orders table and link it with that bill.
									 */
									if(keepAdding == 2) {
										// check if bill is not null to ensure that the user is not checking out an empty cart.
										if(bill != null) {
											// call checkout method and get back orders
											List<Order> orders = customerController.checkout(cart, bill, user, connection);	
											
											System.out.println("\nPurchase complete...");
											
											// print the final bill using orders
											Display.printFinalBill(orders);
										} else {
											System.out.println("Cart is empty. Add some items before checking out.");
										}
									
									// user decides to logout
									} else if (keepAdding == 3) {
										User loggedOutUser = customerController.logout(user);
										System.out.println("User "+loggedOutUser.getUserName()+" logged out");
										break loop;
									
									// user entered an invalid option.
									} else if(keepAdding != 2 && keepAdding != 3 && keepAdding != 1) {
										System.out.println("Please enter a valid option.");
										break;
									}
								} while(keepAdding == 1);
								
							// user decides to not to start adding items into cart, so log the user out and close the application.
							case 2:
								break productSwitchCase;
							default:
								System.out.println("Please enter a valid option");
								break;
						}
					}
					
					break;
				// user decides to logout
				case 2:
					User loggedOutUser = customerController.logout(user);
					System.out.println("User "+loggedOutUser.getUserName()+" logged out");
					break loop;
				default:
					System.out.println("Please select a valid option.");
					break;
			}
			
			System.out.println("\nDo you want to continue using the app? Choose(1/2)");
			System.out.println("1. Yes");
			System.out.println("2. No");
			try {
				decision = input.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("Please only use 1 or 2 to select the option.");
				System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
			}
		} while(decision == 1);	
	}

}

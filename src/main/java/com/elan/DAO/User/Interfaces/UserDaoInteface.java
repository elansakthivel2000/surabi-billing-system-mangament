package com.elan.DAO.User.Interfaces;

import java.sql.Connection;

import com.elan.Model.User.User;

public interface UserDaoInteface {
	User login(User user, Connection connection);
	User register(User user, Connection connection);
	User logout(User user);
}

package com.elan.DAO.User.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.elan.DAO.User.Interfaces.UserDaoInteface;
import com.elan.Model.User.User;
import com.elan.Model.User.UserType;

/*
 * 		UserDAO has methods which interact with users table in our database.
 */

public class UserDAO implements UserDaoInteface {
	
	/**
	 * 
	 * 	This method is used to login an already existing user.
	 * 	@param user- User object which has username and password value.
	 * 	@param connection - Connection object to perform jdbc operation.
	 *  @return User object if successful, otherwise null.
	 * 
	 */
	@Override
	public User login(User user, Connection connection) {
//		System.out.println(">> UserDAO: Inside Login method."); (FOR DEV PURPOSE)
		String query = "SELECT * FROM users WHERE username=? AND password=?";
		try {
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query);
		
			// set the values dynamically from User object into prepared statement.
			statement.setString(1, user.getUserName());
			statement.setString(2, user.getPassword());	
			
			// execute the query.
			ResultSet resultSet = statement.executeQuery();
			
			// check if the user exist 
			// if resultSet has an entry user exists, otherwise user doesn't exist login should fail.
			if(resultSet.isBeforeFirst()) {
				
				// user exists, move the cursor to point to that entry
				resultSet.next();
				
				// get the role (USER or ADMIN) and id from the table and set it to our User Object.
				String role = resultSet.getString("role");
				int id = resultSet.getInt("id");
				
				user.setLoggedIn(true);
				user.setId(id);
				if(role.equals("ADMIN")) {
					user.setUserType(UserType.ADMIN);
				}
				
				// return the user
				return user;
			}
			
			// if user does not exist return null.
			return null;
			
		} catch (SQLException e) {
			System.out.println(">>UserDAO: Inside Login Method Catch Block, Something went wrong with login.");
			System.out.println(e.getMessage());
//			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * 	This method is used to register a new user.
	 * 	@param user- User object which has username and password value.
	 * 	@param connection - Connection object to perform jdbc operation.
	 *  @return User object if successful, otherwise null.
	 * 
	 */
	@Override
	public User register(User user, Connection connection) {
//		System.out.println(">> UserDAO: Inside Register method."); (FOR DEV PURPOSE)
		String query = "INSERT INTO users (username, password) VALUES (?, ?)";
		
		try {
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			// set the values dynamically from User object into prepared statement.
			statement.setString(1, user.getUserName());
			statement.setString(2, user.getPassword());
			
			// if the insertion is successful result will be 1.
			int result = statement.executeUpdate();
			
			if(result == 1) {
				// get the result table to traverse and perform operations.
				ResultSet resultSet = statement.getGeneratedKeys();
				
				// move the cursor to the first entry. (only one entry will be there, because we're performing only one insertion).
				resultSet.next();
				
				// get the newly generated id and link it with our user object.
				int id = (int) resultSet.getInt(1);
				user.setId(id);
				
				// mark user as logged in
				user.setLoggedIn(true);
				
				// return user
				return user;
			}
			
			// return null if insertion fails
			return null;
			
		} catch (SQLException e) {
			System.out.println(">>UserDAO: Inside Register Method Catch Block, Something went wrong with Register.");
			System.out.println(e.getMessage());
//			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * 
	 * 	This method is used to logout a user.
	 * 	@param user- User object which has username and password value.
	 *  @return User object if successful, otherwise null.
	 * 
	 */
	@Override
	public User logout(User user) {
		if (user.isLoggedIn()) {
			user.setLoggedIn(false);
			return user;
		}
		return null;
	}
}

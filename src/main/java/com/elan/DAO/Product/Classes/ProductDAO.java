package com.elan.DAO.Product.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.elan.DAO.Product.Interfaces.ProductDaoInterface;
import com.elan.Model.Product.Product;

/*
 * 		ProductDAO has methods which interact with products table in our database.
 */

public class ProductDAO implements ProductDaoInterface {
	
	/**
	 * 
	 * 	This method is used to get all the products from products table by user.
	 * 	@param connection - Connection object to perform jdbc operation.
	 *  @return list of Product object if there's any entry in products table in database otherwise null.
	 * 
	 */
	@Override
	public List<Product> getAllProducts(Connection connection) {
//		System.out.println(">> ProductDAO: Inside getAllProducts method."); (FOR DEV PURPOSE)
		String query = "SELECT * FROM products";
		try {
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query);
			
			// get the result table to traverse and perform operations.
			ResultSet resultSet = statement.executeQuery();
			
			// create an arraylist of Product object to store all the products.
			List<Product> productsList = new ArrayList<>();
			
			// traverse through all the tables by moving the cursor.
			while(resultSet.next()) {
				int id = resultSet.getInt("id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("price");
			
				// create Product object using values got from database
				Product product = new Product(id, productName,productPrice);
				
				// add the product to products list.
				productsList.add(product);
			}
			
			// return null if there are no products in database otherwise return products list.
			return productsList.size() == 0 ? null : productsList;
			
		} catch (SQLException e) {
			System.out.println(">>ProductDAO: Inside getAllProducts method Catch Block, Something went wrong.");
			System.out.println(e.getMessage());
//			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * 	This method is used to add a new product to the databse by admin.
	 * 	@param connection - Connection object to perform jdbc operation.
	 * 	@param product - Product object to add to database.
	 *  @return list of Product object if there's any entry in products table in database otherwise null.
	 * 
	 */
	@Override
	public Product addNewProduct(Product product, Connection connection) {
		String query = "INSERT INTO products (product_name, price) VALUES (?,?)";
		
		
		try {
			// create a prepared statement to perform insertion operation
			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			// set the values dynamically from product object to prepared statement
			statement.setString(1, product.getName());
			statement.setDouble(2, product.getPrice());
			
			// if the insertion is successful result will be 1.
			int result = statement.executeUpdate();
			
			if(result == 1) {
				// get the result table to traverse and perform operations.
				ResultSet resultSet = statement.getGeneratedKeys();
				
				// move the cursor to the first entry. (only one entry will be there, because we're performing only one insertion).
				resultSet.next();
				
				// get the newly generated id.
				int id = (int) resultSet.getInt(1);
				product.setId(id);
				
				// return product
				return product;
			}
			
			// return null if insertion fails
			return null;
			
		} catch (SQLException e) {
			System.out.println(">>ProductDAO: Inside addNewProduct method Catch Block, Something went wrong with inserting new product.");
			System.out.println(e.getMessage());
			return null;
		}
	}

}

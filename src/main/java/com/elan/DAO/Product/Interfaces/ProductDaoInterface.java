package com.elan.DAO.Product.Interfaces;

import java.sql.Connection;
import java.util.List;

import com.elan.Model.Product.Product;

public interface ProductDaoInterface {
	List<Product> getAllProducts(Connection connection);
	Product addNewProduct(Product product, Connection connection);
}

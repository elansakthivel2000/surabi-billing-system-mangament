package com.elan.DAO.Order.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elan.DAO.Order.Interfaces.OrderDaoInterface;
import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;

/*
 * 		OrderDAO has methods which interact with orders table in our database.
 */
public class OrderDAO implements OrderDaoInterface {

	/**
	 * 
	 * 	This method is used to create a order entry in the database orders table.
	 * 		- when a user checksout his/her cart, first a bill entry will be created with no.of items in cart and total price.
	 * 		- then for each item in the cart seperate entry will be created in the orders table, this is to enable the ability
	 *        to find all the products assosiated with a bill.
	 * 
	 * 	@param productCountMap - HashMap with Product as key and its count as value.
	 * 			prodcuctCountMap structure
	 * 			{
	 * 				{name:pizza, price: 12.99} : 2,
	 * 				{name:pasta, price: 3.12}: 1
	 * 			}	
	 * 
	 *  @param bill - Bill object which has bill's id to be linked with a order.
	 *  @param connection - Connection object to perform jdbc operation.
	 *  @return lists of Order when the cart is not empty and null if the cart is empty.		 
	 * 
	 */
	@Override
	public List<Order> placeOrder(HashMap<Product, Integer> productCountMap, Bill bill, Connection connection) {
//		System.out.println(">> OrderDAO: Inside placeOrder method."); (FOR DEV PURPOSE)
		
		String query = "INSERT INTO orders (product_id, bill_id, product_count) VALUES (?,?,?)";
		try {
			
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			// create an arraylist of Order object to store all the orders.
			List<Order> orders = new ArrayList<>();
			
			// loop through the product count map and add an entry in orders table for each item in tha map.
			for(Map.Entry<Product, Integer> entry: productCountMap.entrySet()) {
				Product product = entry.getKey();
				int productCount = entry.getValue();

				statement.setInt(1, product.getId());
				statement.setInt(2, bill.getId());
				statement.setInt(3, productCount);
				
				// add values dynamically to the prepared stament from productCount map then insert it into the table.
				int result = statement.executeUpdate();
				
				if(result == 1) {
					ResultSet resultSet = statement.getGeneratedKeys();
					resultSet.next();
					int id = (int) resultSet.getInt(1);
					
					// add order to orders list.
					orders.add(new Order(id, productCount, product, bill));
				}
			}
			
			// return null if the cart is empty, otherwise return list of orders.
			return orders.size() == 0 ? null : orders;
		} catch (SQLException e) {
			System.out.println(">> OrderDAO: Inside placeOrder method catch block.");
			System.out.println(e.getMessage());
//			e.printStackTrace();
			return null;
		}	
	}
}

package com.elan.DAO.Order.Interfaces;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;

public interface OrderDaoInterface {
	List<Order> placeOrder(HashMap<Product, Integer> productCountMap, Bill bill, Connection connection);
}

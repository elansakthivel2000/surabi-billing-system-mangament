package com.elan.DAO.Bill.Classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.elan.DAO.Bill.Interfaces.BillDaoInterface;
import com.elan.Model.Bill.Bill;
import com.elan.Model.SalesReport.Report;
import com.elan.Model.User.User;

/*
 * 		BillDAO has methods which interact with bills table in our database.
 */

public class BillDAO implements BillDaoInterface {

	/**
	 * 
	 * 	This method is used to create a bill entry in the database bill table.
	 * 	@param bill - Bill object which holds the amount and num of items purchased.
	 *  @param user - User object which has user's id to be linked with bill.
	 *  @return Bill object if a bill is successfully created in database otherwise null.		 
	 * 
	 */
	@Override
	public Bill purchase(Bill bill, User user, Connection connection) {
//		System.out.println(">> BillDAO: Inside purchase method");	(FOR DEV PURPOSE)
		String query = "INSERT INTO bills (bill_amount, num_items, purchased_by) VALUES (?,?,?)";
		
		try {
		
			// create prepared statement to perform insert operation
			// to get back the inserted result, use Statement.RETURN_GENERATED_KEYS
			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			
			// set the values dynamically from Bill object into prepared statement.
			statement.setDouble(1, bill.getAmount());
			statement.setInt(2, bill.getNumberOfItems());
			statement.setInt(3, user.getId());
			
			// if the insertion is successful result will be 1.
			int result = statement.executeUpdate();
			
			if(result == 1) {
				// get the result table to traverse and perform operations.
				ResultSet resultSet = statement.getGeneratedKeys();
				
				// move the cursor to the first entry. (only one entry will be there, because we're performing only one insertion).
				resultSet.next();
				
				// get the newly generated id for bills entry from database and link it with our Bill object.
				int id = (int) resultSet.getInt(1);
				bill.setId(id);
				
				// return the bill
				return bill;
			}	
			
			// if insertion is not successfull return null.
			return null;
		} catch (SQLException e) {
			System.out.println(">> BillDAO: Inside purchase method catch block");
			System.out.println(e.getMessage() != null ? e.getMessage() : "Error inserting into bills table.");
			
//			e.printStackTrace();	(FOR DEV PURPOSE)
			
			// returning null incase of errors.
			return null;
		}
	}

	/**
	 * 
	 * 	This method is used to get all the bills for the current day by admin.
	 * 	@param connection - Connection object to perform jdbc operation.
	 *  @return list of Bill object if there's any entry in bills table in database otherwise null.
	 * 
	 */
	@Override
	public List<Bill> getAllBillsForToday(Connection connection) {
//		System.out.println(">> BillDAO: Inside getAllBillsForToday method"); (FOR DEV PURPOSE)
		
		String query = "SELECT * FROM bills JOIN users ON bills.purchased_by = users.id WHERE DATE(purchased_at) = CURDATE()";
		
		// create an arraylist of Bill object to store all the bills.
		List<Bill> bills = new ArrayList<>();
		try {
			
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query);
			
			// get the result table to traverse and perform operations.
			ResultSet resultSet = statement.executeQuery();
			
			// check if the result table is empty (0 rows)
			// we can check it by checking if the cursort is before the first row.
			if(resultSet.isBeforeFirst()) {
				
				// traverse through all the tables by moving the cursor.
				while(resultSet.next()) {
					
					// retrieve the columns from database and form a User and Bill object.
					int userId = resultSet.getInt("users.id");
					String userName = resultSet.getString("username");
							
					// create User object
					User user = new User();
					user.setId(userId);
					user.setUserName(userName);
					
					int billId = resultSet.getInt("bills.id");
					double billAmount = resultSet.getDouble("bill_amount");
					int numItems = resultSet.getInt("num_items");
					
					// create Bill object
					Bill bill = new Bill(billAmount, numItems, user);
					bill.setId(billId);
					
					// add Bill to our arraylist.
					bills.add(bill);
				}	
			}
			
			// return null if there are no purchases today (bills list is empty) otherwise return bills list.
			return bills.size() == 0 ? null : bills;
			
		} catch(SQLException e) {
//			System.out.println(">> BillDAO: Inside getAllBillsForToday method catch block"); (FOR DEV PURPOSE)
			System.out.println(e.getMessage());
//			e.printStackTrace(); (FOR DEV PURPOSE)
			return null;
		}
	}

	/**
	 * 
	 * 	This method is used to get total sales for the month by admin.
	 * 	@param connection - Connection object to perform jdbc operation.
	 *  @return Report object if there are any purchases in that particular month otherwise null.
	 * 
	 */
	@Override
	public Report getTotalSalesForThisMonth(Connection connection) {
//		System.out.println(">> BillDAO: Inside getTotalSalesForThisMonth method"); (FOR DEV PURPOSE)
		
		String query = "SELECT \r\n"
				+ "    MONTHNAME(purchased_at) AS month,\r\n"
				+ "    YEAR(purchased_at) AS year,\r\n"
				+ "    SUM(bill_amount) AS total_sale\r\n"
				+ "FROM bills\r\n"
				+ "WHERE MONTHNAME(purchased_at) = MONTHNAME(CURDATE()) \r\n"
				+ "GROUP BY year,month";
		
		try {
			// create prepared statement to perform query operation
			PreparedStatement statement = connection.prepareStatement(query);
			
			// get the result table to traverse and perform operations.
			ResultSet resultSet = statement.executeQuery();
			
			// check if the result table is empty (0 rows)
			// we can check it by checking if the cursort is before the first row.
			if(resultSet.isBeforeFirst()) {
				resultSet.next();
				
				// retrieve the columns from database and form a Report object.
				int year = resultSet.getInt("year");
				String month = resultSet.getString("month");
				double totalSale = resultSet.getDouble("total_sale");
				
				// return the report object.
				return new Report(year, month, totalSale);
			}
			
			// return null if there are no purchases the entire month.
			return null;
			
		} catch (SQLException e) {
			System.out.println(">> BillDAO: Inside getAllBillsForToday method catch block");
			System.out.println(e.getMessage());
//			e.printStackTrace(); (FOR DEV PURPOSE)
			return null;
		}
	}
}








package com.elan.DAO.Bill.Interfaces;

import java.sql.Connection;
import java.util.List;

import com.elan.Model.Bill.Bill;
import com.elan.Model.SalesReport.Report;
import com.elan.Model.User.User;

public interface BillDaoInterface {
	Bill purchase (
		Bill bill,
		User user,
		Connection connection
	);
	
	List<Bill> getAllBillsForToday(Connection connection);
	
	Report getTotalSalesForThisMonth(Connection connection);
}

package com.elan.Model.Order;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Product.Product;

/*
 * 	Order Model has bunch of properties and getter setter methods related to order entity
 */

public class Order {
	private int id;
	private int productCount;
	private Product product;
	private Bill bill;

	public Order(int id, int productCount, Product product, Bill bill) {
		this.id = id;
		this.productCount = productCount;
		this.product = product;
		this.bill = bill;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getProductCount() {
		return productCount;
	}
	
	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Bill getBill() {
		return bill;
	}
	
	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
	@Override
	public String toString() {
		return "Order [id=" + id + ", productCount=" + productCount + ", product=" + product + ", bill=" + bill + "]";
	}	
}

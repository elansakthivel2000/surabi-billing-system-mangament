package com.elan.Model.SalesReport;

/*
 * 	Report Model has bunch of properties and getter setter methods to map the montly sales report.
 */

public class Report {
	private final int year;
	private final String month;
	private final double totalSale;
	
	public Report(int year, String month, double totalSale) {
		this.year = year;
		this.month = month;
		this.totalSale = totalSale;
	}

	public int getYear() {
		return year;
	}

	public String getMonth() {
		return month;
	}

	public double getTotalSale() {
		return totalSale;
	}

	@Override
	public String toString() {
		return "Report [year=" + year + ", month=" + month + ", totalSale=" + totalSale + "]";
	}
}

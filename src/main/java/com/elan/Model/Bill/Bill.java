package com.elan.Model.Bill;

import com.elan.Model.User.User;

/*
 * 	Bill Model has bunch of properties and getter setter methods related to bill entity
 */

public class Bill {
	private int id;
	private double amount;
	private int numberOfItems;
	private User purchasedBy;

	public Bill(double amount, int numberOfItems, User purchasedBy) {
		this.amount = amount;
		this.numberOfItems = numberOfItems;
		this.purchasedBy = purchasedBy;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public int getNumberOfItems() {
		return numberOfItems;
	}
	
	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	
	public User getPurchasedBy() {
		return purchasedBy;
	}
	
	public void setPurchasedBy(User purchasedBy) {
		this.purchasedBy = purchasedBy;
	}

	@Override
	public String toString() {
		return "Bill [id=" + id + ", amount=" + amount + ", numberOfItems=" + numberOfItems + ", purchasedBy="
				+ purchasedBy + "]";
	}	
}

package com.elan.Factory.Controller;

import com.elan.DAO.Bill.Classes.BillDAO;
import com.elan.DAO.Order.Classes.OrderDAO;
import com.elan.DAO.Product.Classes.ProductDAO;
import com.elan.DAO.User.Classes.UserDAO;
import com.elan.Surabi.Controllers.Classes.AdminController;
import com.elan.Surabi.Controllers.Classes.CustomerController;
import com.elan.Surabi.Controllers.Classes.UserController;

/*
 * 	ControllerFactory has a method to get Controllers based on ControllerType Enum input.
 * 		- FACTORY DESIGN PATTERN
 * 		- Hides the implementation details from user. (hides all DAO classes from user)
 * 		- We'll assign the controller (which has bunch of methods to perform operation) based on his role
 * 			- CUSTOMER -> customer controller
 * 			- ADMIN -> admin controller
 * 			- USER -> user controller, which is a base class extended by customer and admin controller.
 * */

public class ControllerFactory {
	
	public static UserController getController(ControllerType controllerType) {	
		UserDAO userDao = new UserDAO();
		ProductDAO productDao = new ProductDAO();
		OrderDAO orderDao = new OrderDAO();
		BillDAO billDao = new BillDAO();
		
		// based on type return appropriate controller.
		switch(controllerType) {
			case USER:
				return new UserController(userDao);
			case CUSTOMER:
				return new CustomerController(userDao, productDao, orderDao, billDao);
			case ADMIN:
				return new AdminController(userDao,billDao,productDao);
			default:
				return null;
		}
	}
}

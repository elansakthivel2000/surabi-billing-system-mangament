package com.elan.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * 	SINGLETON DESIGN PATTERN
 * 
 * 	ThreadSafeConnectionSingleton has methods to get a singleton connection to our database which is also thread safe.
 * 		- Only one connection object (resource) for all the threads.
 * 		- After one thread creates it, all the other thread trying to create a new connection object will fail and it'll return the 
 * 		  same instance created by thread one.
 * 
 * 
 * */
public class ThreadSafeConnectionSingleton {
	private static ThreadSafeConnectionSingleton INSTANCE;
	
	// DATABASE CONFIG
	private static String connectionUrl = "jdbc:mysql://localhost:3306/Surabi?useSSL=false&serverTimeZone=UTC";
	private static String user = "glearning";
	private static String password = "glearning";
	public static Connection connection;
    
	private ThreadSafeConnectionSingleton() {
		
	}
	
	/***
	 * 	This method is to create a thread safe singleton connection object using double locked method.
	 * 	@return singleton connection object which is also thread safe.
	 */
	public static Connection getConnection() {
		// enter only if an instance is not created, otherwise use that instance.
		if(INSTANCE == null) {
			// any thread who wants to execute this block of code should have the lock.
			synchronized (ThreadSafeConnectionSingleton.class) {
				if(INSTANCE == null) {
					try {
						// if an instance is not created, create it and store it in INSTANCE
						INSTANCE = new ThreadSafeConnectionSingleton();
						
						// also store the connection object.
						connection = DriverManager.getConnection(connectionUrl,user,password);
						System.out.println("Database Connection successful.");
					} catch (SQLException e) {
//						e.printStackTrace(); (FOR DEV PURPOSE)
						System.out.println("Error Connecting with database.");
						System.out.println(e.getMessage());
					}
				}
			}
		}
		
		// return the connection.
		return connection;
	}
}

package com.elan.Testing;

import com.elan.DAO.User.Classes.UserDAO;
import com.elan.Database.ThreadSafeConnectionSingleton;
import com.elan.Model.User.User;

/*
 * 	Used for testing individual DAO methods (Not related with application).
 */

public class UserDaoTesting {
	public static void main(String[] args) {
		
		// USER DAO TESTING
		
		UserDAO userDAO = new UserDAO();
		User user = new User();
		user.setUserName("Michael");
		user.setPassword("michael#1234");
		
		// REGISTER
		
		User registeredUser = userDAO.register(user, ThreadSafeConnectionSingleton.getConnection());
		System.out.println(registeredUser);
		
		// LOGIN
		
//		User retrievedUser = userDAO.login(user, ThreadSafeConnectionSingleton.getConnection());
//		System.out.println(retrievedUser);
		
		// LOGOUT
	
//		System.out.println(userDAO.logout(user));
		
		System.out.println("Done..");
	}
}

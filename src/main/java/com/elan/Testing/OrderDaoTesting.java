package com.elan.Testing;

import java.util.List;
import java.util.HashMap;

import com.elan.DAO.Order.Classes.OrderDAO;
import com.elan.DAO.Product.Classes.ProductDAO;
import com.elan.Database.ThreadSafeConnectionSingleton;
import com.elan.Model.Bill.Bill;
import com.elan.Model.Product.Product;
import com.elan.Model.User.User;

/*
 * 	Used for testing individual DAO methods (Not related with application).
 */

public class OrderDaoTesting {
	public static void main(String[] args) {
		// ORDER DAO TESTING
		User user = new User();
		user.setId(2);
		user.setUserName("Pranav");
		user.setPassword("pranav#1234");
		
		Bill bill = new Bill(16.20, 2, user);
		bill.setId(2);
		
		ProductDAO productDao = new ProductDAO();
		
		List<Product> products = productDao.getAllProducts(ThreadSafeConnectionSingleton.getConnection());
		
		HashMap<Product, Integer> map = new HashMap<>();
		
		for(Product product:products) {
			map.put(product, map.getOrDefault(product, 0) + 1);
		}
		
		// PLACE ORDER
		OrderDAO orderDao = new OrderDAO();
		System.out.println(orderDao.placeOrder(map, bill, ThreadSafeConnectionSingleton.getConnection()));
	}
}

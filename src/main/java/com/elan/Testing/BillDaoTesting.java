package com.elan.Testing;

import java.util.List;

import com.elan.DAO.Bill.Classes.BillDAO;
import com.elan.Database.ThreadSafeConnectionSingleton;
import com.elan.Model.Bill.Bill;
import com.elan.Model.SalesReport.Report;
import com.elan.Model.User.User;

/*
 * 	Used for testing individual DAO methods (Not related with application).
 */

public class BillDaoTesting {
	public static void main(String[] args) {
		// BILL DAO TESTING
		User user = new User();
		user.setId(1);
		user.setUserName("Elan");
		user.setPassword("elan#1234");
		
		Bill bill = new Bill(16.20, 1, user);
//		
		BillDAO billDao = new BillDAO();
		
//		 PURCHASE
		Bill retrievedBill = billDao.purchase(bill, user, ThreadSafeConnectionSingleton.getConnection());
		System.out.println(retrievedBill);
		
		// GET ALL BILLS FOR TODAY
		List<Bill> bills = billDao.getAllBillsForToday(ThreadSafeConnectionSingleton.getConnection());
		System.out.println(bills);
		
		// GET TOTAL SALE OF THE CURRENT MONTH
		Report report = billDao.getTotalSalesForThisMonth(ThreadSafeConnectionSingleton.getConnection());
		System.out.println(report);
	}
}

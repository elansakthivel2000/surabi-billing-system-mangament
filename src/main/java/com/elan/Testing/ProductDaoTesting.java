package com.elan.Testing;

import com.elan.DAO.Product.Classes.ProductDAO;
import com.elan.Database.ThreadSafeConnectionSingleton;

/*
 * 	Used for testing individual DAO methods (Not related with application).
 */

public class ProductDaoTesting {
	public static void main(String[] args) {
		// PRODUCT DAO TESTING
		
		// GET ALL PRODUCTS
		ProductDAO productDao = new ProductDAO();
		System.out.println(productDao.getAllProducts(ThreadSafeConnectionSingleton.getConnection()));
	}
}

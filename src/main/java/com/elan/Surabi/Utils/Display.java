package com.elan.Surabi.Utils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;
import com.elan.Model.SalesReport.Report;
import com.elan.Model.User.User;

/*
 * 		Display has methods which prints different kind of data structure to console.
 */

public class Display {
	
	/**
	 * 
	 * 	This method is used to print list of products.
	 * 	@param products - list of Product object.
	 *  @return false if products is null indicating nothing to print, true if printed successfully.		 
	 * 
	 */
	public static boolean displayProducts(List<Product> products) {
		// check if product is null, if so return true
		if(products == null) {
			System.out.println("No Products exists.");
			return true;
		}
		
		// otherwise loop through the list and print
		int i = 1;
		System.out.println("\nProducts List");
		for(Product product: products) {
			System.out.println(i+". "+product.getName()+" - "+"$"+product.getPrice());
			i++;
		}
		return false;
	}

	/**
	 * 
	 * 	This method is used to print items in the cart.
	 * 	@param cart - product, count map (to see the structure of cart refer:- OrderDAO).
	 * 	@param user - User Object to create Bill Object (Bill should belong to a User).
	 *  @return null if cart is empty otherwise create a Bill using the items in the cart and return it.		 
	 * 
	 */
	public static Bill displayCart(HashMap<Product, Integer> cart, User user) {
		// check if cart is empty, if so return null
		if(cart.isEmpty() || cart == null) {
			System.out.println("\nCart is Empty.. Start adding items");
			return null;
		}
		
		// other wise loop through and print the cart
		double totalAmount = 0;
		int totalItems = 0;
		System.out.println("\n============= Cart =============\n");
		for(Map.Entry<Product, Integer> entry: cart.entrySet()) {
			Product product = entry.getKey();
			int count = entry.getValue();
			double price = count * product.getPrice();
			System.out.println(product.getName()+" - "+count+" Nos"+" - "+"$"+price);
			totalAmount += price;
			totalItems += count;
		}
		
		// use decimal format to format the decimal digits to have only two digits after point.
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		System.out.println("Total Amount: $"+numberFormat.format(totalAmount));
		System.out.println("\n================================");
		
		// create a new bill with the totalAmount, totalItems got from cart and then return it.
		return new Bill(totalAmount, totalItems, user);
	}

	/**
	 * 
	 * 	This method is used to print the final bill after user checksout.
	 * 	@param orders - list of Order object to get the product and Nos (count of product) to print in bill.
	 *  @return		 
	 * 
	 */
	public static void printFinalBill(List<Order> orders) {
		// if no orders print no itesm purchased and return
		if(orders == null) {
			System.out.println("No items purchased.");
			return;
		}
		
		// otherwise loop through the orders and start printing bill
		System.out.println("\nYour Bill");
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		double totalAmount = 0;
		for(Order order:orders) {
			int prodCount = order.getProductCount();
			double price = prodCount * order.getProduct().getPrice();
			totalAmount += price;
			System.out.println(order.getProduct().getName()+" - "+prodCount+" Nos"+"- $"+numberFormat.format(price));
		}
		System.out.println("Total Amount: $"+numberFormat.format(totalAmount));
	}
	
	/**
	 * 
	 * 	This method is used to print the total bills today when requested by admin.
	 * 	@param bills - list of Bill object to get the bill details.
	 *  @return		 
	 * 
	 */
	public static void displayBills(List<Bill> bills) {
		// if no bills print No Purchases today and return
		if(bills == null) {
			System.out.println("No Purchases today.");
			return;
		}
		
		// otherwise loop through and start printing the bills.
		System.out.println("\nToday's Bills: (Bill id | Total No. Of Items | Amount | Purchased By");
		for(Bill bill:bills) {
			System.out.println(bill.getId()+" - "+bill.getNumberOfItems()+" Nos - $"+bill.getAmount()+" - "
						+bill.getPurchasedBy().getUserName());
		}
	}

	/**
	 * 
	 * 	This method is used to print the monthly report requested by admin.
	 * 	@param report - Report object which contains data about the monthly report.
	 *  @return		 
	 * 
	 */
	public static void displayReport(Report report) {
		// if null print No Purchases this entire month and return.
		if(report == null) {
			System.out.println("No Purchases this entire month.");
			return;
		}
		
		// otherwise print the montly report.
		System.out.println("\nMonth's Sales Report:");
		System.out.println(report.getYear()+" - "+report.getMonth()+" - $"+report.getTotalSale());
	}

	/**
	 * 
	 * 	This method is used to print the monthly report requested by admin.
	 * 	@param product - Product object which contains data about the newly added product.
	 *  @return		 
	 * 
	 */
	public static void displayProducts(Product product) {
		// if null print No product to print and return.
		if(product == null) {
			System.out.println("No Product to print.");
			return;
		}
		
		System.out.println(product.getName()+" - "+product.getPrice()+" added to database.");
	}
}












package com.elan.Surabi.Utils;

import java.sql.Connection;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Classes.UserController;

/**
 * 
 * 	This method is used to get the data required for register.
 * 	@param userController - userController has method regiter which the user can use to regiter.
 * 	@param connection - connection object to perform jdbc operation.
 * 	@param input - to get inputs from the user.
 *  @return User if successful otherwise null.		 
 * 
 */
public class Register {
	public static User register(UserController userController, Connection connection, Scanner input) {
		String userName;
		String password;
		
		try {
			System.out.println("Enter your Username: ");
			userName = input.next();
			
			System.out.println("Enter your Password: ");
			password = input.next();
			
			if(userName.isBlank() || password.isBlank()) {
				System.out.println("Username and Password should not be empty.");
				return null;
			}
			
			return userController.register(new User(userName,password), connection);
		} catch(InputMismatchException e) {
			System.out.println("Invalid entry.");
			System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
			return null;
		}
	}
}

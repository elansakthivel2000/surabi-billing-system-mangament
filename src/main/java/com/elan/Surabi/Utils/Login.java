package com.elan.Surabi.Utils;

import java.sql.Connection;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Classes.UserController;

public class Login {
	
	/**
	 * 
	 * 	This method is used to get the data required for login.
	 * 	@param userController - userController has method login which the user can use to login.
	 * 	@param connection - connection object to perform jdbc operation.
	 * 	@param input - to get inputs from the user.
	 *  @return User if successful otherwise null.		 
	 * 
	 */
	public static User login(UserController userController,Connection connection, Scanner input) {
		String userName;
		String password;
		
		try {
			// get username and password from user.
			System.out.println("Enter your Username: ");
			userName = input.next();
				
			System.out.println("Enter your Password: ");
			password = input.next();
					
			// call userController's login method by passing the username and password
			return userController.login(new User(userName,password), connection);
		} catch(InputMismatchException e) {
			System.out.println("Invalid entry.");
			System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
			return null;
		}
	}
}

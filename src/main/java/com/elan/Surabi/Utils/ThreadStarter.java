package com.elan.Surabi.Utils;

import java.sql.Connection;
import java.util.Scanner;

import com.elan.Factory.Controller.ControllerFactory;
import com.elan.Factory.Controller.ControllerType;
import com.elan.Model.User.User;
import com.elan.Model.User.UserType;
import com.elan.Surabi.Controllers.Classes.AdminController;
import com.elan.Surabi.Controllers.Classes.CustomerController;
import com.elan.Thread.AdminThread;
import com.elan.Thread.CustomerThread;


public class ThreadStarter {

	/**
	 * 
	 * 	This method is used to create and start the thread once a user is logged in or signed up (for every new user).
	 * 	@param user - User Object to pass to CustomerThread.
	 * 	@param connection - connection object to perform jdbc operation.
	 * 	@param input - to get inputs from the user.
	 *  @return		 
	 * 
	 */
	public static void startThreadForUser(User user,Connection connection, Scanner input) {
		if(user.getUserType() == UserType.USER) {
			
			// create a controller based on user's role.
			CustomerController customerController = (CustomerController) ControllerFactory.getController(ControllerType.CUSTOMER);
			
			// create a thread based on user's role
			CustomerThread customerThread = new CustomerThread(customerController,user, connection, input);
			Thread thread = new Thread(customerThread);
			
			/*
			 * 	- Starting the thread.
			 * 	- Thread's run method will be called by the JVM when scheduler schedules this thread.
			 * 	- This thread will be automatically closed once it finished executing its run method.
			 */
			thread.start();
			try {
				// telling the main thread to wait until this thread is finished.
				thread.join();
			} catch (InterruptedException e) {
				System.out.println("Customer thread got interrupted");
//				e.printStackTrace(); (FOR DEV PURPOSE)
				System.out.println(e.getMessage());
			}			
		} else {
			// create a controller based on user's role.
			AdminController adminController = (AdminController) ControllerFactory.getController(ControllerType.ADMIN);
			
			// create a thread based on user's role
			AdminThread adminThread = new AdminThread(adminController,user, connection, input);
			Thread thread = new Thread(adminThread);
			
			/*
			 * 	- Starting the thread.
			 * 	- Thread's run method will be called by the JVM when scheduler schedules this thread.
			 * 	- This thread will be automatically closed once it finished executing its run method.
			 */
			thread.start();
			try {
				// telling the main thread to wait until this thread is finished.
				thread.join();
			} catch (InterruptedException e) {
				System.out.println("Customer thread got interrupted");
//				e.printStackTrace(); (FOR DEV PURPOSE)
				System.out.println(e.getMessage());
			}
		}
	}
}

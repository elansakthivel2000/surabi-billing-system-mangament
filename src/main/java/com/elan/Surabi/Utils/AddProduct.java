package com.elan.Surabi.Utils;

import java.sql.Connection;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.elan.Model.Product.Product;
import com.elan.Surabi.Controllers.Classes.AdminController;

/**
 * 
 * 	This method is used to get the data for adding product.
 * 	@param adminController - userController has method addProduct which the admin can used to add a new product.
 * 	@param connection - connection object to perform jdbc operation.
 * 	@param input - to get inputs from the user.
 *  @return Product if successful otherwise null.		 
 * 
 */
public class AddProduct {
	public static Product addProduct(AdminController adminController, Connection connection, Scanner input) {
		String name;
		double price;
		
		try {
			System.out.println("Enter Product Name: ");
			name = input.next();
			
			System.out.println("Enter Product Price: ");
			price = input.nextDouble();
			
			if(name.isBlank()) {
				System.out.println("Product Name should not be empty.");
				return null;
			}
			
			if(price < 0) {
				System.out.println("Product price should be positive");
				return null;
			}
			
			Product product = new Product();
			product.setName(name);
			product.setPrice(price);
			
			return adminController.addProduct(connection,product );
		} catch(InputMismatchException e) {
			System.out.println("Invalid entry.");
			System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
			return null;
		}
	}
}

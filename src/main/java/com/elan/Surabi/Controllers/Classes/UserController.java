package com.elan.Surabi.Controllers.Classes;

import java.sql.Connection;

import com.elan.DAO.User.Classes.UserDAO;
import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Interfaces.UserControllerInterface;

/*
 * 	UserController acts as a base class for both admin and customer controller.
 * 	It implements methods from UserControllerInterface
 * 
 * 		FACADE DESIGN PATTERN
 * 			- The idea of providing seperate controllers for user to use, is to hide the complex implementation and 
 * 			  provide an easy interface for use.
 * 			- without these controller user had to use methods from all the DAO directly.
 */
public class UserController implements UserControllerInterface {
	private UserDAO userDao;
	
	public UserController(UserDAO userDao) {
		this.userDao = userDao;
	}

	@Override
	public User login(User user, Connection connection) {
		return userDao.login(user, connection);
	}

	@Override
	public User register(User user, Connection connection) {
		return userDao.register(user, connection);
	}

	@Override
	public User logout(User user) {
		return userDao.logout(user);
	}

}

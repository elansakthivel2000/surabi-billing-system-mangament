package com.elan.Surabi.Controllers.Classes;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import com.elan.DAO.Bill.Classes.BillDAO;
import com.elan.DAO.Order.Classes.OrderDAO;
import com.elan.DAO.Product.Classes.ProductDAO;
import com.elan.DAO.User.Classes.UserDAO;
import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;
import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Interfaces.CustomerControllerInterface;

/*
 * 	CustomerController extends from UserController.
 * 	It implements methods from CustomerControllerInterface.
 *  FOLLOWS FACADE DESIGN PATTERN
 *  	REFER:- UserController for explanation
 */

public class CustomerController extends UserController implements CustomerControllerInterface {
	private ProductDAO productDao;
	private OrderDAO orderDao;
	private BillDAO billDao;
	
	public CustomerController(UserDAO userDao, ProductDAO productDao, OrderDAO orderDao, BillDAO billDao) {
		super(userDao);
		this.productDao = productDao;
		this.orderDao = orderDao;
		this.billDao = billDao;
	}

	@Override
	public List<Product> getProducts(Connection connection) {
		return productDao.getAllProducts(connection);
	}

	@Override
	public List<Order> checkout(HashMap<Product, Integer> productCountMap, Bill bill, User user, Connection connection) {
		Bill currentBill = billDao.purchase(bill, user, connection);
		return orderDao.placeOrder(productCountMap, currentBill, connection);
	}

}

package com.elan.Surabi.Controllers.Classes;

import java.sql.Connection;
import java.util.List;

import com.elan.DAO.Bill.Classes.BillDAO;
import com.elan.DAO.Product.Classes.ProductDAO;
import com.elan.DAO.User.Classes.UserDAO;
import com.elan.Model.Bill.Bill;
import com.elan.Model.Product.Product;
import com.elan.Model.SalesReport.Report;
import com.elan.Surabi.Controllers.Interfaces.AdminControllerInterface;

/*
 * 	AdminController extends from UserController.
 * 	It implements methods from AdminControllerInterface.
 *  FOLLOWS FACADE DESIGN PATTERN
 *  	REFER:- UserController for explanation
 */

public class AdminController extends UserController implements AdminControllerInterface {
	private BillDAO billDao;
	private ProductDAO productDao;
	
	public AdminController(UserDAO userDao, BillDAO billDao, ProductDAO productDao) {
		super(userDao);
		this.billDao = billDao;
		this.productDao = productDao;
	}

	public List<Bill> getTodaysBills(Connection connection) {
		return billDao.getAllBillsForToday(connection);
	}
	
	public Report getMonthsTotalSale(Connection connection) {
		return billDao.getTotalSalesForThisMonth(connection);
	}

	@Override
	public Product addProduct(Connection connection, Product product) {
		return productDao.addNewProduct(product, connection);
	}
}

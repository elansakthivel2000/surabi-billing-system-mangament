package com.elan.Surabi.Controllers.Interfaces;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Order.Order;
import com.elan.Model.Product.Product;
import com.elan.Model.User.User;

public interface CustomerControllerInterface {
	List<Product> getProducts(Connection connection);
	List<Order> checkout(HashMap<Product, Integer> productCountMap, Bill bill, User user,Connection connection);
}

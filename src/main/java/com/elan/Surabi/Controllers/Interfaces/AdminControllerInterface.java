package com.elan.Surabi.Controllers.Interfaces;

import java.sql.Connection;
import java.util.List;

import com.elan.Model.Bill.Bill;
import com.elan.Model.Product.Product;
import com.elan.Model.SalesReport.Report;

public interface AdminControllerInterface {
	List<Bill> getTodaysBills(Connection connection);
	Report getMonthsTotalSale(Connection connection);
	Product addProduct(Connection connection, Product product);
}

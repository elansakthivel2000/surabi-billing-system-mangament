package com.elan.Surabi;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.elan.Database.ThreadSafeConnectionSingleton;
import com.elan.Factory.Controller.ControllerFactory;
import com.elan.Factory.Controller.ControllerType;
import com.elan.Model.User.User;
import com.elan.Surabi.Controllers.Classes.UserController;
import com.elan.Surabi.Utils.Login;
import com.elan.Surabi.Utils.Register;
import com.elan.Surabi.Utils.ThreadStarter;

public class App {
    public static void main(String[] args) throws SQLException {
    	
    	// implemented using try-with-resources block, so these resources will be automatically closed, no need for finally block.
        try (
        	// get database connection using our own singleton implementation
    		Connection connection = ThreadSafeConnectionSingleton.getConnection();
        		
        	// create scanner object to get inputs from user.
            Scanner input = new Scanner(System.in);
        ) {
        	System.out.println("Please Select An Option: ");
        	System.out.println("1. Login");
        	System.out.println("2. Register");
        	
        	int mainOption;
        	UserController userController = ControllerFactory.getController(ControllerType.USER);
        	mainOption = input.nextInt();
        	
    		User user;
        	switch(mainOption) {
        		// if user picks 1, perform login operation using controller interface provided.
        		case 1:
	        		user = Login.login(userController,connection, input);
	        		if(user != null) {
	        			System.out.println(user.getUserName()+" Succesfully logged in.");
	        			
	        			// start thread for each new user logging in
	        			// once the run method of that thread is executed, it'll get closed automatically 
	        			ThreadStarter.startThreadForUser(user, connection, input);
	        			
	        			// if a user closes the application without logging out, set the state of that user to be logged out
	        			if(user.isLoggedIn()) {
	        				System.out.println("Logging user out...");
	        				User loggedOutUser = userController.logout(user);
	    					System.out.println("User "+loggedOutUser.getUserName()+" logged out");
	        			}
	        		} else {
	        			System.out.println("Login failed. Please check your username and password.");
	        		}
	        		break;
        		// if user picks 2, perform register operation using controller interface provided.
        		case 2:
        			user = Register.register(userController, connection, input);
        			if(user != null) {
        				System.out.println(user.getUserName()+" Succesfully signed up and logged in to proceed purchasing.");
        				
        				// start thread for each new user signing up and automatically log in that user
	        			// once the run method of that thread is executed, it'll get closed automatically 
        				ThreadStarter.startThreadForUser(user, connection, input);
        				
        				// if a user closes the application without logging out, set the state of that user to be logged out
        				if(user.isLoggedIn()) {
	        				System.out.println("Logging user out...");
	        				User loggedOutUser = userController.logout(user);
	    					System.out.println("User "+loggedOutUser.getUserName()+" logged out");
	        			}
        			} else {
        				System.out.println("Signup failed. Please check your username and password.");
        			}
        			break;
        		default:
        			System.out.println("Please enter a valid option.");
        			break;
        	}
        	
        	System.out.println("Closing Application..");
        } catch(InputMismatchException e) {
        	System.out.println("Invalid Option: Please only use 1 or 2 to select the option.");
        	System.out.println("Error message: "+e.getMessage() != null ? e.getMessage() : "Input Mismatch");
        } catch(Exception e) {
        	System.out.println("Something went wrong...");
        	System.out.println(e.getMessage());
        }
    }
}







